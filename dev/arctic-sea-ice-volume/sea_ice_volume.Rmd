---
title: |
  | Arctic
  | Sea Ice Volume
pagetitle: "Arctic Sea Ice Volume"
author: "Matthias Schlögl"
date: "`r Sys.setlocale('LC_TIME', 'C'); format(Sys.time(), '%d %B, %Y')`"
output:
  rmdformats::readthedown:
    self_contained: true
    lightbox: true
    df_print: paged
    toc_depth: 3
    toc_float:
      collapsed: false
      smooth_scroll: true
---

# Introduction

<div align="center">
<figure align="center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Aerial_view_of_the_edge_of_the_ice_in_Nunavut_2.jpg/1280px-Aerial_view_of_the_edge_of_the_ice_in_Nunavut_2.jpg" alt="Nunavut" width="400"/>
<figcaption align = "center"><b>Fig. 1: Aerial view of the edge of the ice in Nunavut.</b>

(Slick-o-bot 2013; [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/deed.en). [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Aerial_view_of_the_edge_of_the_ice_in_Nunavut_2.jpg)).
</figcaption>
</figure>
</div>

This is a quick visualization of the arctic sea ice volume. Data is taken from the [PIOMAS Arctic Sea Ice Volume Reanalysis](http://psc.apl.uw.edu/research/projects/arctic-sea-ice-volume-anomaly/) as provided by the Polar Science Center. Sea Ice Volume is calculated using the Pan-Arctic Ice Ocean Modeling and Assimilation System ([PIOMAS](http://psc.apl.uw.edu/research/projects/projections-of-an-ice-diminished-arctic-ocean/), [Zhang and Rothrock, 2003](https://doi.org/10.1175/1520-0493(2003)131<0845:MGSIWA>2.0.CO;2)) developed at APL/PSC. Anomalies for each day are calculated relative to the average over the 1979 -2020 period for that day of the year to remove the annual cycle. 

<div align="center">
<figure align="center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Arctic_Ice_%2819424872258%29.jpg/1280px-Arctic_Ice_%2819424872258%29.jpg" alt="Arctic Ice" width="400"/>
<figcaption align = "center"><b>Fig. 2: Arctic Ice.</b>

(Fæ 2015; [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/deed.en).
[Wikimedia Commons](<https://commons.wikimedia.org/wiki/File:Arctic_Ice_(19424872258).jpg>)).
</figcaption>
</figure>
</div>

# Setup and data preparation
```{r, options, echo=FALSE}
options(tidyverse.quiet = TRUE)
```

First we need some technical setup, including loading packages, defining custom colors, and setting the locale to `en_US.UTF-8` to ensure reproducibility across different systems when working with month names.

```{r, setup, warn=FALSE, results='hide'}
# set locale
Sys.setlocale("LC_ALL", "en_US.UTF-8")

# load packages
library(tidyverse)
library(glue)
```

Data can be downloaded from <http://psc.apl.uw.edu/research/projects/arctic-sea-ice-volume-anomaly/data/>. Note that R supports reading data directly from URLs.

```{r, read-prepare-data}
# read data from url
piomas <- "http://psc.apl.uw.edu/wordpress/wp-content/uploads/schweiger/ice_volume/PIOMAS.2sst.monthly.Current.v2.1.txt"
asiv_wide <- read_table(
  url(piomas), col_names = c("year", month.abb), show_col_types = FALSE
) %>%
  mutate(year = as.integer(year))

asiv_long <- asiv_wide %>%
  # wide to long
  pivot_longer(names_to = "month", -year) %>%
  mutate(month = factor(month, levels = month.abb)) %>%
  mutate(month = as.numeric(month)) %>%
  group_by(year) %>%
  # add zeroth month to make spiral actually spiral
  complete(month = seq(0, 12, 1)) %>%
  ungroup() %>%
  fill(value, .direction = "down") %>%
  arrange(year, month) %>%
  # drop NA values
  mutate(value = ifelse(value == -1, NA, value)) %>%
  drop_na() %>%
  # add 2 data points in first frame for geom_line to work
  mutate(frame = lag(row_number())) %>%
  fill(frame, .direction = "up")

now <- as.integer(format(Sys.time(), "%Y"))
```

# Vizualization

## Plotting function
```{r, plotting-function}
plot_spiral <- function(dat) {
  subtitle <- glue("1979-{now} | Status: {sprintf('%02.0f', tail(dat$month, 1))}/{tail(dat$year, 1)}")
  ggplot(dat, aes(x = month, y = value)) +
    geom_hline(yintercept = seq(0, 35, by = 5), colour = "grey55", size = 0.35) +
    geom_vline(xintercept = 1:12, colour = "grey55", size = 0.35) +
    geom_line(aes(colour = year, group = year)) +
    coord_polar(theta = "x") +
    scale_x_continuous(
      name = "Month",
      limits = c(0, 12),
      breaks = seq(1, 12, 1),
      labels = month.abb
    ) +
    scale_y_continuous(name = "Sea Ice Volume [10³ km³]", limits = c(0, 35)) +
    scale_color_viridis_c(
      name = "Year", option = "cividis", limits = c(1979, now), breaks = seq(1970, now, 10)
    ) +
    ggtitle(label = "Arctic Sea Ice Volume", subtitle = subtitle) +
    theme_linedraw() +
    theme(
      panel.border = element_blank(),
      panel.grid = element_blank(),
      panel.ontop = FALSE,
      text = element_text(
        family = "carlito",
        colour = "black",
        size = 16
      )
    ) +
    guides(color = guide_colourbar(barheight = 15))
}
```

## Static spiral

This is a static visualization of the entire evolution from 1979 up to now.

```{r, static-plot}
plot_spiral(asiv_long)
```

## Dynamic spiral

While [`gganimate`](https://gganimate.com/) provides a nice out-of-the-box solution for animated plots in the ggplot-framework, I noticed that this may be cumbersome when working with larger data sets. I do therefore use a two-step workaround consisting of (i) plotting single time slices first and (ii) then concatenating the single images via `ffmpeg`.

```{r, dynamic-plot, eval=FALSE}
for (i in 1:(nrow(asiv_long) - 1)) {
  if (i %% 10 == 0) {
    print(glue("{Sys.time()} -- {i}/{nrow(asiv_long)}"))
  }
  cnt <- sprintf("%03.0f", i)
  p <- asiv_long %>%
    filter(frame <= i) %>%
    plot_spiral()
  ggsave(
    filename = glue("../../plt/sea_ice_volume_dynamic_{cnt}.png"),
    width = 150, height = 150, units = "mm", dpi = 300
  )
}
```

```{sh, eval=FALSE}
ffmpeg -framerate 5 -pattern_type glob -i '*.png' -c:v libx264 -r 30 arctic_sea_ice_volume.mp4
```

🎞️ [Link to ️video](https://edrop.zamg.ac.at/owncloud/index.php/s/qkPNqjJtLkW46gJ) 🎞

## Session information 
```{r, sessioninfo}
sessioninfo::session_info()
```
